# Standalone Java scripts to encrypt and decrypt data in a Base64 encoded package.

Encrypts some data and packages it into a base64 encoded string for easy distribution.

Encrypts the data with a random 256bit AES key.

Encrypts the AES key with the RSA private key and puts the encrypted AES key in the package along with the AES encrypted data.
Anyone with access to the public key can then decrypt the package. 
However, if an optional package key is provided then the key is further encrypted with 256bit AES encryption based on the provided package key.
In that case the recipient needs both the public key and the package key, to decrypt the package.

This is ideal for a software licence file where RSA pair associated with a product, and, optionally, the package key is associated with the purchaser.
The encrypted data could be a json or properties file, specifying the licence and decoded by the users' software. 

## Generate RSA keys
```shell
java GenerateRSAKeys.java
```
Generates 2048bit RSA key pair and puts them in files `privateKey` and `publicKey`

## Make Encrypted Package
```shell
java EncryptPackage.java > package.txt
```
```shell
java EncryptPackage.java -packageKey=MY-SECRET-PACKAGE-KEY -privateKeyFile=privateKey -dataFile=data.json
```
Encrypts contents of file `data.json` with RSA `privateKey` and AES packageKey `MY-SECRET-PACKAGE-KEY` and outputs a base64 encoded encrypted package.

Other properties can be added with the `-metaProperty.` argument prefix to the unencrypted part of the package, visible when base64 decoded.

E.g.
```shell
% cat data.json 
{
  "field1": "value1",
  "field2": "value2",
  "field3": "value3",
  "sub": {
    "field4": "value4",
    "field5": "value5",
    "field6": "value6"
  }
}

% java EncryptPackage.java -packageKey=MY-SECRET-PACKAGE-KEY -metaProperty.hello=world -metaProperty.tags=rsa,aes > package.txt
```
The package is a Base64 encoded Java properties file format by default.
```shell
% cat package.txt 
X3BrPXRydWUKX2t5PVZSeXdMeGtQVmlFOTdVclFySmkycG1oN3E3VlVDcDhhNVFaaWxidVVSek5yWUVtNUVNSS9LZzEwaXBWVFJJYUNoaE1heGtHS0ZvU05Zd3d3ZUtVVTF1dGFKcWVCTGdwMThFdlM4MHVqQk9NQmF1Q29UemVEai9IeThLVzJmYUNtdFQ1emtWcElSNERVNGZLZ2NOZ3B1bUREditUcktLbmlOc0p4ZE1CdkRsYTFWQ25uNUNLR01IZnoyejY3TlZzVGxXQXkva2xST3Nqdmg5S0RkSzJMT0IwTEFmRUV4ejd0TUNrWnp0R0VRcUI5NW1XaFZMeHB4ZjZIR1k0RGhRckNkSVVUazBpRTZETEFUUDNOYThxM1daRjV1enNFM2NwS0ZwNGs3QVB3VVo3VnZFWGIrYmd3cE0rMEd0djB4cWtmU0ZGVnE4NDFWQ2pObi9pMXpJMGRRSmpYdFlTK29od3RRR0kwdWlSM1VJTW5Ha1FKYkNDVGc5blBrdzVGSHJYWUlUNXg3TWR3aGtsaWRkeUcKX2RhPVFaWG1wS3hGV2xTa3htUVlTWUtTWldyeUJPWTlRSUk2MTk3Ymt2QzgwaVVLOHpJVjY2YUVUb2p6QzJlTWVVUHUwaVBkNGFHS3I5M2RJbjBiaE12VXhlenBwTkxCNFJpMjM2Rnl3ajVEc1NpYUg4M3FBU004WFVkYTRSYnozZllTSDltZmhOV2lrQ2Eva2tTYzRqVWVxQlFqbDRSMVVCem84bmM4Wmdmam4zeDVFZXBQUTcxcDV5ZGlDb21DdkR4eTJOcnM2ZEZ2RlMvZzdYVDB6YlRCRFE9PQp0YWdzPXJzYSxhZXMKaGVsbG89d29ybGQK

% base64 -d -i package.txt
_key=See36uZ6/LZqwUXWL/ozIWlTx6nqSQ4FvUN+8vsDLE2D5dRRFrDCF94EclCJwZbJRi3GeO0dcsHTQChGyDOwuXYWEuHsIkX+JCyPc61v24NcxDtUebIWZhHe2W37fFbCZvvVi+0Y5pFdDpiVAt+6Nqd3GGu6gyaQqADyn/dX3Pq4sAuKBGbe1dhR+unZQKp1CpS5G+ev0AVm4munBz3sDVEcv8/besr4CZ3d3IBzKLQqSd+2yJ6mlZowAMSqklIg4cdP9hACg9boNAd2jWbBiJ5Ey8jCwcglUakUyIrpajtxJURmfGan04b2cUs1c2LPlNlJPStRoRXfT2qtd6f/Zov6zjusOhcOEr6pw3ojkbhSGNsQDgqHNBlE4FG3MhYPJPD0rRZnKsJqdEFW
_data=pSJYtXhe03wu9ROOrRoHj9yFaIWJj4um+w90ZIPflTu6q3KzsXGxdRlFADcwMEUaURFKinkS3C/RtnAkTJYrG1TZ/LB9OV0/VrbiS/9erRE5QyG3j2ZqS+W9pnaNXBm4ohTRyqqnF8/29bb6Dty3+Q4uIuGVcygQnp/fiiGJHOD3Fc70bFFk2NvvEWqIXXLb1kP0q/9+oB75RocO9FcSsQ==
hello=world
tags=rsa,aes
```
Or it can be in `JSON` format with the `packageAs=JSON` options.
```shell
% java EncryptPackage.java -packageKey=MY-SECRET-PACKAGE-KEY -metaProperty.hello=world -metaProperty.tags=rsa,aes -packageAs=JSON > package.txt

% cat package.txt
ewoiX2tleSI6Ii9uaDgrMlVBdCtZb3lwMFdSeWI3ekhZSGVDNzJESWQvZ3V1N2xnc2R5Z0Z5bUhHVy91bzVzeDRSV1NiWHNDaHQ5M2dMR3kxK0J4SXR6OERacktNRG5UNFJOS3JkSmk0bFN5dXo5ZDliMC8zQlV0YklkWFY0bmdtU1JTSlp2V3k0aGg2amRXTFNpMVBvajJBMHZkSlFHUWlOSVFlb1FoQjYxKzhLdjh5Nzl5OTVzMWkyLzNDWDJKNVhkSGF1OGhjblRYMy9WWGxnNld5VXhLY04wU2tIOUMvaU5jVHpORVg0Sk9WeWFzUWJGVXRIVzJLTXMwczZlRFZBNE05NEh4RDVCZTh3OXBodXh5OUZlb2hBWThNRmNxdTVTeWc5K3BtYTRrbXovb0xVVmgxVWxBczBuYWRLYXdIeUpHV05KRnFjY2w0dnRWa1ltTTA4Q003d2JxRkhlblhjQURmdlRTUGF0eThHRkl2OEdaS3ZDUmxKbE1SS0p6aFJSNkdRYi9sYXdjekIvYzM3VFV4UkFXZ04iLAoiX2RhdGEiOiI2NStZTitua3NqMjViLzFTeUFpTS9sblc1KzFBR2lranM5ZmtTWUEwd3FhcUovMHVtcy9CVWR3U1hhcTZMRmx4R3Q0UGNJRHhjSmRlWW5KQlhUcFp3WGxPNEEvVG1vZmV0YUlEZUh2ZzFjY0NSem8xcmlrWGJsbDBWUzRPZG4rVE41djJTNGdtK1JJNmtKcFkzM3B4Yys1aEVqczlqSkZvajQ3eGpPUGt2V2xLU2NVcFBJd2VOb2V4SGJSeVJ1Y0l0QkNDdWQvbHNOallIRnJSdXNnNGpBPT0iLAoiaGVsbG8iOiJ3b3JsZCIsCiJ0YWdzIjoicnNhLGFlcyIKfQ==

% base64 -d -i package.txt
{
"_key":"/nh8+2UAt+Yoyp0WRyb7zHYHeC72DId/guu7lgsdygFymHGW/uo5sx4RWSbXsCht93gLGy1+BxItz8DZrKMDnT4RNKrdJi4lSyuz9d9b0/3BUtbIdXV4ngmSRSJZvWy4hh6jdWLSi1Poj2A0vdJQGQiNIQeoQhB61+8Kv8y79y95s1i2/3CX2J5XdHau8hcnTX3/VXlg6WyUxKcN0SkH9C/iNcTzNEX4JOVyasQbFUtHW2KMs0s6eDVA4M94HxD5Be8w9phuxy9FeohAY8MFcqu5Syg9+pma4kmz/oLUVh1UlAs0nadKawHyJGWNJFqccl4vtVkYmM08CM7wbqFHenXcADfvTSPaty8GFIv8GZKvCRlJlMRKJzhRR6GQb/lawczB/c37TUxRAWgN",
"_data":"65+YN+nksj25b/1SyAiM/lnW5+1AGikjs9fkSYA0wqaqJ/0ums/BUdwSXaq6LFlxGt4PcIDxcJdeYnJBXTpZwXlO4A/TmofetaIDeHvg1ccCRzo1rikXbll0VS4Odn+TN5v2S4gm+RI6kJpY33pxc+5hEjs9jJFoj47xjOPkvWlKScUpPIweNoexHbRyRucItBCCud/lsNjYHFrRusg4jA==",
"hello":"world",
"tags":"rsa,aes"
}
```
The metaProperties properties in JSON will always be strings, regardless of value.

The `_data` property value is a Base64 encoded binary value containing the AES encrypted data.

The `_key` property value is a Base64 encoded binary value containing the AES key to the `_data` property.

The `_key` property value is RSA encrypted. (And AES package encrypted - if `-packageKey` provided)

Instead of providing a `-dataFile` to encrypt, you can provide a number of properties that will be encrypted as a Java properties file.
The arguments must have the prefix `-dataProperty.` for it to be identified as a property to encrypt.

E.g.
```shell
% java EncryptPackage.java -dataProperty.SomeKey1=SomeValue2 -dataProperty.SomeKey2=SomeValue2 > package.txt
% java DecryptPackage.java                                                                                  
SomeKey1=SomeValue2
SomeKey2=SomeValue2
```
Note: you can not provide both `-dataFile` and `-dataProperty`

## Decrypt Package
```shell
java DecryptPackage.java
```
```shell
java DecryptPackage.java -packageKey=MY-SECRET-PACKAGE-KEY -publicKeyFile=publicKey -packageFile=package.txt
```
Decrypts the contents of file `package.txt` with RSA `publicKey` and AES packageKey `MY-SECRET-PACKAGE-KEY` and outputs the decrypted package data to the console. 

```shell
% java DecryptPackage.java -packageKey=MY-SECRET-PACKAGE-KEY                                            
{
  "field1": "value1",
  "field2": "value2",
  "field3": "value3",
  "sub": {
    "field4": "value4",
    "field5": "value5",
    "field6": "value6"
  }
}
```